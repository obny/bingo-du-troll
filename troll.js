var selected = new Array(5);
var n = 8078;

/* initialise la grille en validant ou non les cases en fonction du contenu de l'ancre */
function init(){
	var hash = window.parent.location.hash.substr(1) || "0"; // récupération de l'ancre
	hash = parseInt(hash,16).toString(2); //conversion
	hash = "0000000000000000000000000".substr(hash.length) + hash; // padding
	for(var i = 0; i < 5; i++){
		selected[i] = new Array(5);
		for(j = 0; j < 5 ; j++){
			if(hash[0] == '1'){
				document.getElementById("circle" + (n - 20*i -4*j)).style.display = 'inline';
				selected[i][j] = true;
			}
			else{
				document.getElementById("circle" + (n - 20*i -4*j)).style.display = 'none';
				selected[i][j] = false;			
			}
			hash = hash.substr(1);
		}
	}
  score();
}

/* sélectionne ou désélectionne une case */
function play(j, i){
  if (selected[i][j]){
    document.getElementById("circle" + (n - 20*i -4*j)).style.display = 'none';
    selected[i][j] = false;
  }
  else{
    document.getElementById("circle" + (n - 20*i -4*j)).style.display = 'inline';
    selected[i][j] = true;
  }
  list_selected();
  score();
}

/* liste les cases validées et change l'ancre en conséquence*/
function list_selected(){
	var enc = "";
	for(var i = 0; i < 5; i++){
		for(var j = 0; j < 5; j++){
			if(selected[i][j]){
				enc = enc + "1";
			}
			else{
				enc = enc + "0";
			}
		}
	}
	var hash = parseInt(enc, 2).toString(16);
	window.parent.location.hash = hash;
}

/* calcule le score et l'affiche */
/* pas optimal, on peut le recalculer localement dans play et init */
function score(){
  var score = 0;
  for(var i = 0; i < 5; i++){
    var colonne = 1;
    for(var j = 0; j < 5; j++){
      if(selected[i][j]){
        score++;
      }
      else{
        colonne = 0;
      }
    }
    score += 5 * colonne;
  }
  for(var j = 0; j < 5; j++){
    var ligne = 1;
    for(var i = 0; i < 5; i++){
      if(!selected[i][j]){
        ligne = 0;
        break;
      }
    }
    score += 5 * ligne;
  }
  var diag = 1;
  for(var k = 0; k < 5; k++){
    if(!selected[k][k]){
      diag = 0;
      break;
    }
  }
  score += 5 * diag;
  diag = 1;
  for(var k = 0; k < 5; k++){
    if(!selected[k][4-k]){
      diag = 0;
      break;
    }
  }
  score += 5 * diag;
  document.getElementById("score").textContent = "Score : " + score;
}

